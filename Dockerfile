FROM node:9.5-alpine

WORKDIR /app

COPY package.json ./
RUN npm install

COPY server.js ./
COPY Procfile ./
COPY app ./app

EXPOSE $PORT

CMD npm start
