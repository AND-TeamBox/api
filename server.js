const express        = require('express');
const bodyParser     = require('body-parser');
var   app            = express();
const router = require('./app/routes');

const allowCrossDomain = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
};

app.use(allowCrossDomain);
router(app);

const port = (process.env.PORT || 8000);

app.use(bodyParser.urlencoded({ extended: true }));

var server = app.listen(port, () => {
	console.log('We are live on ' + port);
});

module.exports = {
    server,
    app
};
