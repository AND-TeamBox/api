var chai = require('chai');
var chaiHttp = require('chai-http');
var {server} = require('../server.js');
var should = chai.should();
var {app} = require('../server.js');
	
chai.use(chaiHttp);

const surveyDetailsResponse = {"response_count":7,"page_count":1,"date_created":"2018-02-23T10:03:00","buttons_text":{"done_button":"Done","prev_button":"Prev","exit_button":"","next_button":"Next"},"folder_id":"0","custom_variables":{},"nickname":"","id":"131121370","question_count":4,"category":"community","preview":"https://www.surveymonkey.com/r/Preview/?sm=CMFxMYLL0NT2A4okbnNrWZm5Qx7miDl40Yxb4zmY1RQLBZTkjNdgjZZD6CjmK0dj","is_owner":true,"language":"en","footer":true,"date_modified":"2018-02-26T14:25:00","analyze_url":"https://www.surveymonkey.com/analyze/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D","pages":[{"href":"https://api.surveymonkey.net/v3/surveys/131121370/pages/68636656","description":"<div>Hi ANDis! We are creating a comprehensive view of skills across the club to help Club Execs place ANDis within the appropriate client. Please can you complete the survey so we can assign you to the right client.</div>","questions":[{"sorting":null,"family":"open_ended","subtype":"single","required":{"text":"This question requires an answer.","amount":"0","type":"all"},"visible":true,"href":"https://api.surveymonkey.net/v3/surveys/131121370/pages/68636656/questions/264778513","headings":[{"heading":"First name: "}],"position":1,"validation":null,"id":"264778513","forced_ranking":false},{"sorting":null,"family":"open_ended","subtype":"single","required":{"text":"This question requires an answer.","amount":"0","type":"all"},"visible":true,"href":"https://api.surveymonkey.net/v3/surveys/131121370/pages/68636656/questions/264871393","headings":[{"heading":"Last Name:"}],"position":2,"validation":null,"id":"264871393","forced_ranking":false},{"sorting":null,"family":"single_choice","subtype":"menu","required":{"text":"This question requires an answer.","amount":"0","type":"all"},"answers":{"choices":[{"visible":true,"text":"Advisory Board","position":1,"id":"1817203694"},{"visible":true,"text":"Basecamp ","position":2,"id":"1817203695"},{"visible":true,"text":"Ada","position":3,"id":"1817203696"},{"visible":true,"text":"Dekker","position":4,"id":"1817203698"},{"visible":true,"text":"Turing","position":5,"id":"1817203700"},{"visible":true,"text":"Kilburn","position":6,"id":"1817203702"}]},"visible":true,"href":"https://api.surveymonkey.net/v3/surveys/131121370/pages/68636656/questions/264778866","headings":[{"heading":"Club:"}],"position":3,"validation":null,"id":"264778866","forced_ranking":false},{"sorting":null,"family":"matrix","subtype":"single","required":null,"answers":{"rows":[{"visible":true,"text":".NET","position":1,"id":"1824045534"},{"visible":true,"text":"C#","position":2,"id":"1824045535"},{"visible":true,"text":"Docker","position":3,"id":"1824045536"},{"visible":true,"text":"Java","position":4,"id":"1824045537"},{"visible":true,"text":"Spring","position":5,"id":"1824045538"},{"visible":true,"text":"Node.JS","position":6,"id":"1824045539"},{"visible":true,"text":"PHP","position":7,"id":"1824045540"},{"visible":true,"text":"AWS","position":8,"id":"1824045541"},{"visible":true,"text":"Azure","position":9,"id":"1824045542"},{"visible":true,"text":"GCP","position":10,"id":"1824045543"},{"visible":true,"text":"SQL","position":11,"id":"1824045544"},{"visible":true,"text":"No-SQL","position":12,"id":"1824045545"},{"visible":true,"text":"Angular 1","position":13,"id":"1824045546"},{"visible":true,"text":"Angular 2, 4 & Typscript","position":14,"id":"1824045547"},{"visible":true,"text":"HTML","position":15,"id":"1824045548"},{"visible":true,"text":"CSS","position":16,"id":"1824045549"},{"visible":true,"text":"LESS","position":17,"id":"1824045550"},{"visible":true,"text":"SASS","position":18,"id":"1824045551"},{"visible":true,"text":"Javascript - FS","position":19,"id":"1824045552"},{"visible":true,"text":"Javascript - BE","position":20,"id":"1824045553"},{"visible":true,"text":"Javascript - FE","position":21,"id":"1824045554"},{"visible":true,"text":"React.JS","position":22,"id":"1824045555"},{"visible":true,"text":"Redux","position":23,"id":"1824045556"},{"visible":true,"text":"Hybris","position":24,"id":"1824045557"},{"visible":true,"text":"Magento","position":25,"id":"1824045558"},{"visible":true,"text":"Hybrid mobile","position":26,"id":"1824045559"},{"visible":true,"text":"Android","position":27,"id":"1824045560"},{"visible":true,"text":"iOS/Swift","position":28,"id":"1824045561"},{"visible":true,"text":"Accessibility","position":29,"id":"1824045562"},{"visible":true,"text":"E-Commerce","position":30,"id":"1824045563"},{"visible":true,"text":"Delivery Management","position":31,"id":"1824045564"},{"visible":true,"text":"Business Analysis","position":32,"id":"1824045566"},{"visible":true,"text":"Business Intelligence","position":33,"id":"1824045568"},{"visible":true,"text":"UX/UI","position":34,"id":"1824045571"},{"visible":true,"text":"Product Owner","position":35,"id":"1824045574"},{"visible":true,"text":"Product Manager","position":36,"id":"1824045576"},{"visible":true,"text":"Scrum/Agile","position":37,"id":"1824045578"},{"visible":true,"text":"Technical Analysis","position":38,"id":"1824045579"},{"visible":true,"text":"User Experience","position":39,"id":"1824045580"},{"visible":true,"text":"Web Analytics","position":40,"id":"1824045581"}],"choices":[{"description":"","visible":true,"id":"1824045582","is_na":false,"text":"Upskill","position":1},{"description":"","visible":true,"id":"1824045583","is_na":false,"text":"Junior","position":2},{"description":"","visible":true,"id":"1824045584","is_na":false,"text":"Senior","position":3},{"description":"","visible":true,"id":"1824045585","is_na":false,"text":"Lead","position":4}]},"visible":true,"href":"https://api.surveymonkey.net/v3/surveys/131121370/pages/68636656/questions/265844591","headings":[{"heading":"<span style=\"font-size: 14pt;\"><span style=\"font-size: 18pt;\">Skillset:</span><br><br><span style=\"font-size: 18pt;\">Please only select one or more skills that apply to you and assign the appropriate experience level to each skill.</span> </span><br><br><em><span style=\"font-size: 10pt;\">Key: </span></em><br>\n<ul>\n<li><em><span style=\"font-size: 10pt;\">Upskill - I would like to develop skills in this area</span></em></li>\n<li><em><span style=\"font-size: 10pt;\">Junior - I am capable of creating high quality work with support and guidance</span></em></li>\n<li><em><span style=\"font-size: 10pt;\">Senior - I am capable of working independently and creating high quality work with limited support</span></em></li>\n<li><em><span style=\"font-size: 10pt;\">Lead - I am highly experienced in leading teams and supporting junior team members</span></em></li>\n</ul>"}],"position":4,"validation":null,"id":"265844591","forced_ranking":false}],"title":"TeamBox - Skillset Survey","position":1,"id":"68636656","question_count":4}],"summary_url":"https://www.surveymonkey.com/summary/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D","href":"https://api.surveymonkey.net/v3/surveys/131121370","title":"ANDi TeamBox","collect_url":"https://www.surveymonkey.com/collect/list?sm=NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D","edit_url":"https://www.surveymonkey.com/create/?sm=NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D"};

const surveyResponses = {"per_page":50,"total":10,"data":[{"total_time":77,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6714100119","custom_variables":{},"ip_address":"88.98.169.97","id":"6714100119","logic_path":{},"date_modified":"2018-02-23T11:33:12+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6714100119","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"John Doe"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-23T11:31:54+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=CKVM7Or6TSCHYqqIl8c32pzMaqX3XZpjn4PRcc4hPnXx8g1DnX6qbf8F9qh0d_2BmV","metadata":{}},{"total_time":36,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6714341740","custom_variables":{},"ip_address":"88.98.169.97","id":"6714341740","logic_path":{},"date_modified":"2018-02-23T13:52:53+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6714341740","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Mario King"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-23T13:52:17+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=_2FxsXANL5Bgv7ufDHjUUyIPatKfSZ4YbD6P_2BTEI3A21TjSe0tXO290OeJy5il8HN_2B","metadata":{}},{"total_time":106,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6714347247","custom_variables":{},"ip_address":"88.98.169.97","id":"6714347247","logic_path":{},"date_modified":"2018-02-23T13:55:24+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6714347247","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Tom Jones"}]},{"id":"264778866","answers":[{"choice_id":"1817203695"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-23T13:53:37+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=Zp08_2Fe9QzLh7qAL0XbfkhFPTyo4akG511S8CV26w1vQf7VRscQiRfmI4oQYWM4ET","metadata":{}},{"total_time":55,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6714349670","custom_variables":{},"ip_address":"88.98.169.97","id":"6714349670","logic_path":{},"date_modified":"2018-02-23T13:56:32+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6714349670","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Tom Hanks"}]},{"id":"264778866","answers":[{"choice_id":"1817203695"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-23T13:55:37+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=LJudA5zIldW4Tt_2B1Ni7Z40oaeh1bUDRqpYWiNrCgfbfqD6oR4_2B_2BEZ_2F0iGeLielEQ","metadata":{}},{"total_time":251,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6714551087","custom_variables":{},"ip_address":"5.148.147.235","id":"6714551087","logic_path":{},"date_modified":"2018-02-23T15:19:09+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6714551087","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Jasmien"}]},{"id":"264871393","answers":[{"text":"Cels"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-23T15:14:58+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=xO_2FOmoy9zPI15gSWqIVLy8WbC5P0yQdMKSyZsAeit8p4xCDX739XSpWi1JSQkvCI","metadata":{}},{"total_time":635,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6718936950","custom_variables":{},"ip_address":"88.98.169.97","id":"6718936950","logic_path":{},"date_modified":"2018-02-26T11:58:30+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6718936950","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Niru"}]},{"id":"264871393","answers":[{"text":"Sharma"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-26T11:47:55+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=ElTFrutmoktAr8Xl_2B3e7XU_2Fqdgtr4mf_2BhfUp_2BBewZDE7Z5EXmbMxZCBD1ioDArIj","metadata":{}},{"total_time":103,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6718940889","custom_variables":{},"ip_address":"88.98.169.97","id":"6718940889","logic_path":{},"date_modified":"2018-02-26T12:01:03+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6718940889","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"William"}]},{"id":"264871393","answers":[{"text":"Orange"}]},{"id":"264778866","answers":[{"choice_id":"1817203698"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-26T11:59:19+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=9RiA84TmlSEDOCs_2BrRRvsz4l42Y9sIMQ7zAC1scGehxAIl_2Ba0wlT57V1AcKZxr4E","metadata":{}},{"total_time":236,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6719365195","custom_variables":{},"ip_address":"88.98.169.97","id":"6719365195","logic_path":{},"date_modified":"2018-02-26T15:13:52+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6719365195","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Watson "}]},{"id":"264871393","answers":[{"text":"Turing"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]},{"id":"265844591","answers":[{"choice_id":"1824045585","row_id":"1824045534"},{"choice_id":"1824045584","row_id":"1824045535"},{"choice_id":"1824045583","row_id":"1824045548"},{"choice_id":"1824045583","row_id":"1824045549"},{"choice_id":"1824045585","row_id":"1824045576"},{"choice_id":"1824045583","row_id":"1824045579"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-26T15:09:56+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=WWrJ0ji7L5_2FiGz_2By3Ee_2FlQ6hdhUkTGEP81fmN26Fp4_2Fsy3Argzv2mIevWTWmrq7q","metadata":{}},{"total_time":321,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6719395180","custom_variables":{},"ip_address":"88.98.169.97","id":"6719395180","logic_path":{},"date_modified":"2018-02-26T15:23:57+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6719395180","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Sherlock"}]},{"id":"264871393","answers":[{"text":"Holmes"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]},{"id":"265844591","answers":[{"choice_id":"1824045584","row_id":"1824045535"},{"choice_id":"1824045585","row_id":"1824045539"},{"choice_id":"1824045582","row_id":"1824045542"},{"choice_id":"1824045585","row_id":"1824045552"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-26T15:18:36+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=R5E3qLtPf8f84nTmIc1Dn4iukMlvW_2BLJMa4_2FORoFuoZ8FOa61eFdYnxutU106peY","metadata":{}},{"total_time":133,"href":"https://api.surveymonkey.net/v3/surveys/131121370/responses/6720065770","custom_variables":{},"ip_address":"77.102.80.129","id":"6720065770","logic_path":{},"date_modified":"2018-02-26T19:17:59+00:00","response_status":"completed","custom_value":"","analyze_url":"https://www.surveymonkey.com/analyze/browse/NFn3wT1BVtftQFK9BcMItuEaH5j1JKNU61oE37r9Ohs_3D?respondent_id=6720065770","pages":[{"id":"68636656","questions":[{"id":"264778513","answers":[{"text":"Giovanni "}]},{"id":"264871393","answers":[{"text":"Toscani"}]},{"id":"264778866","answers":[{"choice_id":"1817203700"}]},{"id":"265844591","answers":[{"choice_id":"1824045582","row_id":"1824045534"},{"choice_id":"1824045582","row_id":"1824045535"},{"choice_id":"1824045582","row_id":"1824045536"},{"choice_id":"1824045582","row_id":"1824045537"},{"choice_id":"1824045582","row_id":"1824045538"},{"choice_id":"1824045583","row_id":"1824045539"},{"choice_id":"1824045583","row_id":"1824045540"},{"choice_id":"1824045582","row_id":"1824045541"},{"choice_id":"1824045582","row_id":"1824045542"},{"choice_id":"1824045582","row_id":"1824045543"},{"choice_id":"1824045583","row_id":"1824045544"},{"choice_id":"1824045582","row_id":"1824045545"},{"choice_id":"1824045583","row_id":"1824045546"},{"choice_id":"1824045583","row_id":"1824045547"},{"choice_id":"1824045584","row_id":"1824045548"},{"choice_id":"1824045584","row_id":"1824045549"},{"choice_id":"1824045584","row_id":"1824045550"},{"choice_id":"1824045584","row_id":"1824045551"},{"choice_id":"1824045583","row_id":"1824045552"},{"choice_id":"1824045583","row_id":"1824045553"},{"choice_id":"1824045583","row_id":"1824045554"},{"choice_id":"1824045583","row_id":"1824045555"},{"choice_id":"1824045582","row_id":"1824045556"},{"choice_id":"1824045582","row_id":"1824045557"},{"choice_id":"1824045582","row_id":"1824045558"},{"choice_id":"1824045582","row_id":"1824045559"},{"choice_id":"1824045582","row_id":"1824045560"},{"choice_id":"1824045582","row_id":"1824045561"},{"choice_id":"1824045583","row_id":"1824045562"},{"choice_id":"1824045583","row_id":"1824045563"},{"choice_id":"1824045583","row_id":"1824045564"},{"choice_id":"1824045583","row_id":"1824045566"},{"choice_id":"1824045583","row_id":"1824045568"},{"choice_id":"1824045584","row_id":"1824045571"},{"choice_id":"1824045582","row_id":"1824045574"},{"choice_id":"1824045583","row_id":"1824045576"},{"choice_id":"1824045582","row_id":"1824045578"},{"choice_id":"1824045582","row_id":"1824045579"},{"choice_id":"1824045584","row_id":"1824045580"},{"choice_id":"1824045583","row_id":"1824045581"}]}]}],"page_path":[],"recipient_id":"","collector_id":"170601296","date_created":"2018-02-26T19:15:45+00:00","survey_id":"131121370","collection_mode":"default","edit_url":"https://www.surveymonkey.com/r/?sm=xh6T6x0QL0U5m1Tb0m7lcAGiby_2FLXcm1ITCIO8qrv7ZV76fcqqKRxT_2BKtwSR6PzH","metadata":{}}],"page":1,"links":{"self":"https://api.surveymonkey.net/v3/surveys/131121370/responses/bulk?page=1&per_page=50"}};

describe('Search', function() {
  	it('It should return a status of OK/200 /search/{skill} GET', function(done) {
	  	chai.request(server)
	    	.get('/search/css')
	    	.end(function(err, res) {
	    	  	res.should.have.status(200);
	    	  	done();
	    	});
	});

	it('should return a json array from /search/{skill} GET', function(done) {
	 	chai.request(server)
	   	 	.get('/search/css')
	   		.end(function(err, res) {
	    		res.should.be.json;
	      		res.body.should.be.a('object');
	      		done();
	    });
	});

	it('should return pass for property fullName', function(done) {
		chai.request(server)
			.get('/search/css')
			.end(function(err, res) {
				res.body.result[0].should.have.property('fullName');
				done();
			});		
	});

	it('should return an array for otherSkills from /search/{skill} GET', function(done) {
	 	chai.request(server)
	   	 	.get('/search/css')
	   		.end(function(err, res) {
	      		res.body.result[0].otherSkills.should.be.a('array');
	      		done();
	    });
	});

	it('should return true that otherSkills contains skill objects from /search/{skill} GET', function(done) {
	 	chai.request(server)
	   	 	.get('/search/css')
	   		.end(function(err, res) {
	      		res.body.result[0].otherSkills[0].should.have.a('object');
	      		done();
	    });
	});


	it('should contain property "skillResult"', function(done) {
		chai.request(server)
			.get('/search/css')
			.end(function(err, res) {
				res.body.result[0].should.have.property('skillResult');
				done();
			});	
	});


	it('should have value "css" for property "skill"', function(done) {
		chai.request(server)
			.get('/search/css')
			.end(function(err, res) {
				res.body.result[0].skillResult.should.have.property('skillName', 'css');
				done();
			});	
	});

	it('should not have value "javascript" for property skill on call for "css"', function(done) {
		chai.request(server)
			.get('/search/css')
			.end(function(err, res) {
				res.body.result[0].skillResult.should.not.have.property('skillName', 'javascript');
				done();
			});
	});

	it('should return array where all users have skillName C#', function(done) {
		chai.request(server)
			.get('/search/c%23')
			.end(function(err, res) {
				for(let i=0; i<res.body.result.length; i++) {
					res.body.result[i].skillResult.should.have.property('skillName', 'c#');
				}
				done();
			});
	});

	it('should return 3 results with skillname either docker or css', function(done) {
		chai.request(server)
			.get('/search/css,docker')
			.end(function(err, res) {
				for(let i=0; i<res.body.result.length; i++) {
					res.body.result[i].skillResult.skillName.should.satisfy(function(skillName) {
						return (skillName == 'docker' || skillName == 'css');
					});
				}
				done();
			});
	});

});
